package com.example.administrator.colorarcprogressbartest;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

public class DashboardView extends View{
    private int len;

    private final float startAngle=45;                                                                //起始角度
    private final float sweepAngle=270;                                                               //经过角度
    private float maxValue = sweepAngle;                                                              //这个控件可以设置的最大值，默认与仪表盘一致
    private float expectValue;                                                                      //实际用户期望达到的值
    float trueAngle = 0;

    private float targetAngle = 0;                                                                    // 刻度经过角度范围
    private Paint paint;
    private int radius;

    private Paint textPaint;                                                                        // 绘制文字

    private int devideNum = 36;                                                                     //刻度的个数
    private int lineWidth = 4;
    private float interval = sweepAngle/devideNum;

    private boolean isRunning;                                                                      //判断是否在动

    public DashboardView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    /**
     *1  初始化画笔对象
     */
    private void initPaint() {
        paint =new Paint();                                                                         //设置画笔颜色

        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);                                                                   //设置画笔抗锯齿
        paint.setStyle(Paint.Style.STROKE);                                                         //让画出的图形是空心的(不填充)

        textPaint = new Paint();                                                                    //文本画笔
        textPaint.setARGB(255, 255, 143, 0);
        textPaint.setAntiAlias(true);
    }

    /**
     *2  来测量限制view为正方形
     * @param widthMeasureSpec 检测的宽度
     * @param heightMeasureSpec 检测出来的高度
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        len = Math.min(width, height);                                                              //以最小值为刻度区域正方形的长
        radius = len/2;

        setMeasuredDimension(len,len);                                                              //设置测量高度和宽度
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawViewLine(canvas);
    }

    /**
     * 绘制刻度线
     * @param canvas
     */
    private void drawViewLine(Canvas canvas) {
        canvas.save();
        canvas.translate(radius,radius);                                                            //移动canvas
        Paint linePatin=new Paint();                                                                //普通刻度

        linePatin.setColor(Color.GRAY);                                                            //设置普通刻度画笔颜色
        linePatin.setStrokeWidth(lineWidth);                                                        //线宽
        linePatin.setAntiAlias(true);                                                               //设置画笔抗锯齿

        float rotateAngle = sweepAngle/(devideNum - 1);                                             //确定每次旋转的角度

        Paint targetLinePatin=new Paint();                                                          //绘制需要有颜色部分的画笔
        targetLinePatin.setColor(Color.GREEN);
        targetLinePatin.setStrokeWidth(lineWidth);
        targetLinePatin.setAntiAlias(true);

        canvas.rotate(startAngle);                                                                  //旋转canvas
        float hasDraw=0;                                                                            //记录已经绘制过的有色部分范围(角度float)

        float percent = 0;                                                                          //计算已经绘制的比例
        for(int i = 0;i < devideNum;i ++){

            if (hasDraw <= targetAngle && targetAngle != 0 ) {                                      //有色和无色分别用不同颜色

                percent = hasDraw/sweepAngle;
                int green= (int) (186 - (43 * percent));                                            //green 186~143

                if (onAngleColorListener!=null){
                    onAngleColorListener.onPercentChange(percent);
                }
                targetLinePatin.setARGB(255,255,green,0);

                canvas.drawLine(0, radius, 0, radius - 20, targetLinePatin);
            } else {
                canvas.drawLine(0,radius,0,radius - 20,linePatin);
            }

            hasDraw += rotateAngle;
            canvas.rotate(rotateAngle);
        }

        if (targetAngle == trueAngle){
            if (onAngleColorListener!=null){
                onAngleColorListener.onPercentChange(expectValue/maxValue);
            }
        }

        canvas.restore();                                                                           //操作完成后恢复状态
    }

    /**
     * 设置仪表盘的最大值
     * @param value 最大值
     */
    public void  setMaxValue(int value){
        if (value <= 0){
            return ;
        }

        maxValue = value;
    }

    /**
     * 5 根据角度变化刻度
     * @param changeValue
     */
    public void changeAngle(final float changeValue) {
        if (isRunning){//如果在动直接返回
            return;
        }

        expectValue = changeValue;

        float radio = changeValue / maxValue;
        if (radio >= 1){
            trueAngle = maxValue;
        }else {
            trueAngle = radio * sweepAngle;
        }
        startAnimator();
    }


    private void startAnimator() {
       final ValueAnimator animator = ValueAnimator.ofInt(0, (devideNum));
        animator.setDuration(1000);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
          public void onAnimationUpdate(ValueAnimator animation) {
                targetAngle += interval;
                if (targetAngle >= trueAngle) {                                                     //如果增加到指定角度
                    targetAngle = trueAngle;
                    isRunning=false;
                }

                invalidate();
            }
      });
       animator.start();
    }

    private OnAngleColorListener onAngleColorListener;

    public void setOnAngleColorListener(OnAngleColorListener onAngleColorListener) {
        this.onAngleColorListener = onAngleColorListener;
    }

    public interface OnAngleColorListener{
        void onPercentChange(float percent);
    }
}
