package com.example.administrator.colorarcprogressbartest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    DashboardView mView;
    RelativeLayout ll_parent;
    int maxValue = 1200;
    int targetValue = 1200;
    TextView tvPercent;

    DecimalFormat df1 = new DecimalFormat("#");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mView = findViewById(R.id.dashView);
        ll_parent = findViewById(R.id.ll_parent);
        tvPercent = findViewById(R.id.tvValue);

        mView.setMaxValue(maxValue);

        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mView.changeAngle(targetValue);
            }
        });
        mView.setOnAngleColorListener(new DashboardView.OnAngleColorListener() {
            @Override
            public void onPercentChange(float percent) {
                String str = df1.format(percent * maxValue);
                tvPercent.setText(str);
            }
        });
    }
}
